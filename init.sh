eval $(cat env)

docker create \
  --name=sickrage \
  --volumes-from sickrage_data \
  -p $PORT:8081 \
  -e PGID=$GROUPID -e PUID=$USERID \
  linuxserver/sickrage
