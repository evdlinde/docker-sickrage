eval $(cat env)
#Create a data volume container with a named volume.
docker create \
  --name=sickrage_data \
  -v sickrage_volume:/config \
  -v sickrage_volume:/downloads \
  -v sickrage_volume:/tv \
  -e PGID=$GROUPID -e PUID=$USERID \
  linuxserver/sickrage echo "Data container"
